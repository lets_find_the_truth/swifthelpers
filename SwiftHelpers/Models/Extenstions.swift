//
//  Extenstions.swift
//  SwiftHelpers
//
//  Created by Kyryl Nevedrov on 02/04/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit

extension UIStoryboard {
    func initFromStoryboard<T>(viewController: T.Type) -> T {
        let viewControllerIdentifier = String(describing: viewController)
        return self.instantiateViewController(withIdentifier: viewControllerIdentifier) as! T
    }
}
