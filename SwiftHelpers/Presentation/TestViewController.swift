//
//  TestViewController.swift
//  SwiftHelpers
//
//  Created by Kyryl Nevedrov on 25/03/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class TestViewController: UIViewController {
    private var disposeBag = DisposeBag()
    var viewModel: SignInViewModel!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    private let imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary //can be camera
        imagePicker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)! //for video
        
//        photoButton.rx.tap.bind(onNext: {
//
//            self.imagePicker.popoverPresentationController?.delegate = self as! UIPopoverPresentationControllerDelegate
//
//            self.present(self.imagePicker, animated: true, completion: nil)
//        }
//        ).disposed(by: disposeBag)
        //or you can use https://github.com/RxSwiftCommunity/RxMediaPicker
        photoButton.rx.tap
            .flatMapLatest { [weak self] _ in
                return UIImagePickerController.rx.createWithParent(self) { picker in
                    picker.sourceType = .photoLibrary
                    picker.allowsEditing = false
                    }
                    .flatMap { $0.rx.didFinishPickingMediaWithInfo }
                    .take(1)
            }
            .map { info in
                return info[UIImagePickerController.InfoKey.originalImage] as? UIImage
            }
            .bind(to: imageView.rx.image)
            .disposed(by: disposeBag)
    }
    
    func showPicker() {
      
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TestViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        /* "UIImagePickerControllerImageURL" file:///Users/admin/Library/Developer/CoreSimulator/Devices/F48AF1ED-1E1E-4558-BF0F-6E2B47101363/data/Containers/Data/Application/C03762F0-A637-49D2-A441-D50E4368E706/tmp/07C0EC8B-7831-49C3-A332-529C0DF991B5.jpeg
         "UIImagePickerControllerMediaType" public.image or public.movie
         "UIImagePickerControllerReferenceURL" assets-library://asset/asset.JPG?id=B84E8479-475C-4727-A4A4-B77AA9980897&ext=JPG
         - key : "UIImagePickerControllerOriginalImage" UIImage */
        //get imageData or videoData
        /*
         let imageURL = info[UIImagePickerControllerImageURL] as! URL
         
         do {
         let imageData = try Data(contentsOf: imageURL)
         } catch let error {} */
        //or UIImageJPEGRepresentation(image, 1.0)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.image = image
        }
        picker.dismiss(animated: true, completion: nil)
    }
}


