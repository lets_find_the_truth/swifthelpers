//
//  UIElementsViewController.swift
//  SwiftHelpers
//
//  Created by Kyryl Nevedrov on 06/03/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class UIElementsViewController: UIViewController {
    @IBOutlet weak var stateChangeSwitch: UISwitch!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var testTextField: UITextField!
    @IBOutlet weak var testLabel: UILabel!
    @IBOutlet weak var testSlider: UISlider!
    @IBOutlet weak var testProgressView: UIProgressView!
    @IBOutlet weak var testButton: UIButton!
    @IBOutlet weak var clickCountLabel: UILabel!
    @IBOutlet weak var firstTextView: UITextView!
    @IBOutlet weak var secondTextView: UITextView!
    @IBOutlet weak var testStepper: UIStepper!
    @IBOutlet weak var stepperLabel: UILabel!
    @IBOutlet weak var testPickerView: UIPickerView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var testDatePicker: UIDatePicker!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    private let disposeBag = DisposeBag()
    private let publish = PublishSubject<String>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        stateChangeSwitch.rx.isOn
            .bind(to: activityIndicator.rx.isAnimating)
            .disposed(by: disposeBag)
        testTextField.rx.text.orEmpty
            .map {
                "->\($0)"
            }
            .bind(to: testLabel.rx.text)
            .disposed(by: disposeBag)
    
        firstTextView.rx.text.orEmpty
            .bind(to: secondTextView.rx.text)
            .disposed(by: disposeBag)
        testTextField.rx.text.orEmpty
            .bind(to: publish)
            .disposed(by: disposeBag)
  
        
        testSlider.rx.value
            .bind(to: testProgressView.rx.progress)
            .disposed(by: disposeBag)
        
        testButton.rx.tap.enumerated()
            .map {
                $0.index + 1
            }
            .bind(onNext: { [weak self] in
                self?.clickCountLabel.text = "Number of clicks: \($0)"
            })
            .disposed(by: disposeBag)
        
        testDatePicker.rx.date
            .map {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd.MM.yyyy"
                return dateFormatter.string(from: $0)
            }
            .bind(to: dateLabel.rx.text)
            .disposed(by: disposeBag)
        
        Observable.just([0, 1, 2, 3, 4, 5, 6, 7, 8]).bind(to: testPickerView.rx.itemTitles) { _, item in
                return "\(item)"
            }.disposed(by: disposeBag)
            
            ///OR
//            .bind(to: testPickerView.rx.itemAttributedTitles) { _, item in
//                return NSAttributedString(string: "\(item)",
//                    attributes: [
//                        NSAttributedString.Key.foregroundColor: UIColor.cyan,
//                        NSAttributedString.Key.underlineStyle: NSUnderlineStyle.double.rawValue
//                    ])
//            }
            
//            .bind(to: pickerView3.rx.items) { _, item, _ in
//                let view = UIView()
//                view.backgroundColor = item
//                return view
//            }

        
            testPickerView.rx.modelSelected(Int.self)
            .subscribe(onNext: { (model) in
                print(model)
            })
            .disposed(by: disposeBag)
        
        segmentedControl.rx.value
            .skip(1)
            .bind(onNext: { (next) in
                print(next)
            })
            .disposed(by: disposeBag)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
