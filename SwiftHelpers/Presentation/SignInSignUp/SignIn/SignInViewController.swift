//
//  SignInViewController.swift
//  SwiftHelpers
//
//  Created by Kyryl Nevedrov on 06/02/2019.
//  Copyright © 2019 Kyryl Nevedrov. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa


class SignInViewController: UIViewController {
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton! 
    private let disposeBag = DisposeBag()
    
    //TODO
    var viewModel: SignInViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        signInButton.rx.tap.bind(onNext:{ [unowned self] in
            print("login")
        }).disposed(by: disposeBag)
        
        emailTextField.rx.text.orEmpty
            .bind(to: viewModel.email)
            .disposed(by: disposeBag)
        passwordTextField.rx.text.orEmpty
            .bind(to: viewModel.password)
            .disposed(by: disposeBag)
        
        viewModel.isValid.subscribe { (isValid) in
            print(isValid)
        }.disposed(by: disposeBag)
        viewModel.isValid.bind(to: signInButton.rx.isEnabled).disposed(by: disposeBag)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
